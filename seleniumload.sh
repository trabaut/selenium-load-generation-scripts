#!/bin/bash

PATH=/home/appddemo:$PATH

_startSeleniumDemoLoadSingleThreaded ()
{
echo "Starting Selenium Demo Load"
python /home/appddemo/src/SeleniumDemoLoad/SeleniumDemoLoad.py /home/appddemo/src/SeleniumDemoLoad/DemoLoadConfigAcme3 > acme3.log &
python /home/appddemo/src/SeleniumDemoLoad/SeleniumDemoLoad.py /home/appddemo/src/SeleniumDemoLoad/DemoLoadConfigAcme4 > acme4.log &
python /home/appddemo/src/SeleniumDemoLoad/SeleniumDemoLoad.py /home/appddemo/src/SeleniumDemoLoad/DemoLoadConfigMovieSearch3 > movies3.log &
python /home/appddemo/src/SeleniumDemoLoad/SeleniumDemoLoad.py /home/appddemo/src/SeleniumDemoLoad/DemoLoadConfigMovieSearch4 > movies4.log &
python /home/appddemo/src/SeleniumDemoLoad/SeleniumDemoLoad.py /home/appddemo/src/SeleniumDemoLoad/DemoLoadConfigStaging > demostaging.log &

return
}

_startSeleniumDemoLoadMultiThreaded ()
{ 
echo "Starting Selenium Demo Load"
python /home/appddemo/src/SeleniumDemoLoad/SeleniumDemoLoad.py /home/appddemo/src/SeleniumDemoLoad/DemoLoadConfig > seleniumdemoload.log &

return
}

_stopSeleniumDemoLoad ()
{
echo "Stopping Selenium Demo Load" 
echo "Killing Selenium Python Script" 
echo `ps -ef | grep SeleniumDemoLoad.py | grep -v grep | awk '{print $2}'`
kill -9 `ps -ef | grep SeleniumDemoLoad.py | grep -v grep | awk '{print $2}'`
echo "Killing Phantom Processes" 
echo `ps -ef | grep phantom | grep -v grep | awk '{print $2}'`
kill -9  `ps -ef | grep phantom | grep -v grep | awk '{print $2}'`
echo "Killing Firefox Processes" 
echo `ps -ef | grep firefox | grep -v grep | awk '{print $2}'`
kill -9  `ps -ef | grep firefox | grep -v grep | awk '{print $2}'`
echo "Killing Chrome Processes" 
echo `ps -ef | grep chromedriver | grep -v grep | awk '{print $2}'`
kill -9  `ps -ef | grep chromedriver | grep -v grep | awk '{print $2}'`
echo "Killing Xvfb Processes" 
echo `ps -ef | grep Xvfb | grep -v grep | awk '{print $2}'`
kill -9  `ps -ef | grep Xvfb | grep -v grep | awk '{print $2}'`
return 
}

_cleanTmpDirectory () 
{ 
echo "Cleaning /tmp Directory" 
rm -fr /tmp/tmp*
rm -fr /tmp/server*
}

_moveSeleniumLog ()
{ 
echo "Moving seleniumdemoload.log" 
rm seleniumdemoload.log.old
mv seleniumdemoload.log seleniumdemoload.log.old
}

_restartSeleniumDemoLoad ()
{
_stopSeleniumDemoLoad 
_cleanTmpDirectory
_moveSeleniumLog
_startSeleniumDemoLoadMultiThreaded 
}

_restartSeleniumDemoLoadAndMachineAgent ()
{ 
_restartSeleniumDemoLoad
echo "Stopping Machine Agent..." 
/home/appddemo/MachineAgent/killMachineAgent.sh
echo "Finished" 
echo "Starting Machine Agent..."
/home/appddemo/MachineAgent/startMachineAgent.sh
echo "Finished"
}

################

case $1 in
    start-single-threaded)
        _startSeleniumDemoLoadSingleThreaded ;;
    start-multi-threaded)
        _startSeleniumDemoLoadMultiThreaded ;;
    stop)
        _stopSeleniumDemoLoad ;;
    restart) 
        _restartSeleniumDemoLoad ;; 
    restart-selenium-machine-agent)
        _restartSeleniumDemoLoadAndMachineAgent ;;
    *)
        echo "usage: seleniumload.sh [start-single-threaded|start-multi-threaded|stop|restart|restart-selenium-machine-agent]" ;;
esac

exit 0
