#!/usr/bin/python
import sys
import threading
import time
import gc
from datetime import datetime
from pyvirtualdisplay import Display
from selenium import webdriver
from selenium.common.exceptions import WebDriverException
from easyprocess import EasyProcessCheckInstalledError
from PHPDemoApp import PHPDemoApp
from AcmeBookStore import AcmeBookStore
from MovieDemoApp import MovieDemoApp



class SeleniumThread(threading.Thread): 
	
    def __init__(self,demoapp,browser,url,path,useragent):
        self.demoapp = demoapp
        self.browser = browser
        self.url = url
        self.path = path
        self.useragent = useragent
        self.display = ""
        threading.Thread.__init__(self)

    def run(self): 
        try:
            driver = self.getDriver()
        
            if self.demoapp == "PHPDemoApp": 
                app = PHPDemoApp(driver,self.url)
                app.run()
            elif self.demoapp == "AcmeBookStore": 
                app = AcmeBookStore(driver,self.url)
                app.run()
            elif self.demoapp == "MovieDemoApp": 
                app = MovieDemoApp(driver, self.url)
                app.run()
    
        except EasyProcessCheckInstalledError as epciError: 
            print "Xvfb is not installed. Unable to start Virtual Display"

    def getDriver(self): 
	
        if self.browser == "Firefox" or self.browser == "Chrome":
            self.display = Display(visible=0, size=(800, 600))
            self.display.start()
	
        try: 
            if self.browser == "Firefox": 
                profile = webdriver.FirefoxProfile()
                profile.set_preference("general.useragent.override", self.useragent)
                #profile.set_preference("general.useragent.override", "Mozilla/5.0 (X11; Linux x86_64; rv:24.0) Gecko/20100101 Firefox/22.0")
		return webdriver.Firefox(profile)
            elif self.browser == "PhantomJS": 
                return webdriver.PhantomJS(self.path) 
            elif self.browser == "Chrome": 
                profile = webdriver.ChromeProfile()
                profile.set_preference("general.useragent.override", self.useragent)
	        return webdriver.Chrome(profile)
        except WebDriverException as wdException: 
            print "SeleniumDemoLoad - WebDriverException: {0}".format(wdException)
            return self.getDriver()

config = sys.argv[1]

settings = list()
setting = list()
lineNum=0

with open("%s" % config) as myfile:
    for line in myfile:
        myvars = {}
        setting = line.split('#')
        for rec in setting:
            print "REC = %s" % rec
            name, var = rec.split('=',2)
            myvars[name.strip()] = var.strip()
        settings.append(myvars)
        
print """settings = %s\n""" % settings

stdoutmutex = threading.Lock()
threads = []
for i in settings: 
    print """URL = %s""" % i['URL']
    thread = SeleniumThread(i['DemoApp'],i['Browser'],i['URL'],i["Path"],i["UserAgent"])
    thread.start()
    threads.append(thread)

for thread in threads: 
    thread.join()
print('Main thread exiting.') 





