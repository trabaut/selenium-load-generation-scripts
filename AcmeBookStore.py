import sys
import gc
import time
import random
from datetime import datetime
from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait

class AcmeBookStore(): 

    def __init__(self,driver,url): 
        self.driver = driver
        self.url = url


    def run(self): 

        count = 1
        max = 10
        # Looping through requests
        while count < max:
            try:
                self.request()
            except NameError as ne:
                print "Acme Online Bookstore - NameError: {0}".format(ne)
                exit()
            except:
                print "Acme Online Bookstore - Unexpected error:", sys.exc_info()[0]

    def request(self): 

        _timeout = 15
        checkoutfrequency = 10

        # request the home page
        self.driver.get(self.url)

        # Login
        print "Acme Book Store: %s" % self.driver.title


        # find the username and password text boxes
        w = WebDriverWait(self.driver, _timeout)
        w.until(lambda driver: self.driver.find_element_by_id("textBox"))
        loginTextBox = self.driver.find_element_by_name('loginName')
        passwordBox = self.driver.find_elements_by_id('password')
        submit = self.driver.find_elements_by_id('UserLogin_Login')

        # check to see if the AppD JavaScript has been injected
        #html_source = self.driver.page_source
        #if "window.ADRUM" not in html_source:
            #print "bad ADRUM"

        if submit[0].is_displayed:
            # Enter username and password
            loginTextBox.send_keys("test")
            passwordBox[0].send_keys("appdynamics")
            # Submit the username and password (login)
            submit[0].click()

            w = WebDriverWait(self.driver, _timeout)
            w.until(lambda driver: self.driver.find_element_by_id("itemsForm_submitValue"))

            # AppDynamics Pilot
            #print self.driver.title

            # Find the Add To Cart button/element
            addCartButton = self.driver.find_elements_by_id('itemsForm_submitValue')
            if addCartButton[0].is_displayed:

                #checkbox = self.driver.find_element_by_xpath("/html/body/table/tbody/tr[1]/td/div/div")
                #checkbox_Obama = self.driver.find_element_by_xpath('//*[@id="selectedId"]')

                # Finding the checkbox for the Obama book
                checkbox_Obama_1=self.driver.find_element_by_xpath('//*[@id="itemsForm"]/table/tbody/tr[1]/td[3]/div/div/input')

                # Generating random number to decide whether to purchase Obama book
                r = random.randint(1,int(checkoutfrequency))
                if r==9:
                    #print "Clicking Obama"
                    checkbox_Obama_1.click()

                #classes = self.driver.find_elements_by_class_name("itemBoxes")
                checkboxes = self.driver.find_element_by_id("selectedId")
                checkboxes.click()

                #print "Clicking Add to Cart Button\n"
                addCartButton[0].click()

                r = random.randint(1,10)
                
                if r == 1: 
                    # Clicking Delete from cart
                    print "Clicking Delete"
                    tags = self.driver.find_elements_by_tag_name('input')
                    tags[5].click()
                else: 
                    t = time.localtime()
                    if t.tm_min > 0 and t.tm_min < 5: 
                        self.driver.execute_script("document.getElementById('orderAmount').value='1'")
                    elif t.tm_min > 29 and t.tm_min < 35: 
                        self.driver.execute_script("document.getElementById('orderAmount').value='1'") 
                    else:
                        self.driver.execute_script("document.getElementById('orderAmount').value='1000'")
                    w = WebDriverWait(self.driver, _timeout)
                    w.until(lambda driver: self.driver.find_element_by_id("ViewCart_submitValue"))

                    # Clicking Checkout
                    self.driver.find_element_by_id("ViewCart_submitValue").click()

                # Finding the logout button
                w = WebDriverWait(self.driver, _timeout)
                w.until(lambda driver: self.driver.find_element_by_xpath('/html/body/table/tbody/tr[1]/td/div/div/div/span'))
                logout = self.driver.find_element_by_xpath("/html/body/table/tbody/tr[1]/td/div/div/div/span")

                # Clicking logout
                logout.click()
        #Performing garbage collectoin 
        collected = gc.collect()
        print "Garbage collector: collected %d objects." % (collected)
        print datetime.now()
