import sys
import gc
import time
import random
from datetime import datetime
from selenium import webdriver
from selenium.webdriver.support.wait import WebDriverWait


class PHPDemoApp(): 

    def __init__(self,driver,url): 
        self.driver = driver
        self.url = url

    def run(self): 

        count = 1
        max = 10
        _timeout = 15
        # Looping through requests
        while count < max:
            try:

                print """PHP Demo App: %s""" % (self.url)

                # request the home page
                self.driver.get(self.url)

                #print self.driver.title

                r1 = random.randint(1,4)

                for i in range(r1):
                    r2 = random.randint(1,4)

                    if r2 == 1:
                        product_name = "A Foo Bar"
                    elif r2 == 2:
                        product_name = "A Foo Bar2"
                    elif r2 == 3:
                        product_name = "Big Tank"
                    elif r2 == 4:
                        product_name = "Product 1"
                    w = WebDriverWait(self.driver, _timeout)
                    w.until(lambda driver: self.driver.find_element_by_link_text(product_name))
                    product = self.driver.find_element_by_link_text(product_name)
                    #print "product = %s" % product_name
                    product.click()

                    w = WebDriverWait(self.driver, _timeout)
                    w.until(lambda driver: self.driver.find_elements_by_tag_name("input"))
                    addtocart = self.driver.find_elements_by_tag_name("input")
                    #print self.driver.page_source
                    #print addtocart
                    #print len(addtocart)
                    #print "add to cart"
                    addtocart[1].click()

                    w = WebDriverWait(self.driver, _timeout)
                    w.until(lambda driver: self.driver.find_element_by_link_text("Home"))
                    home = self.driver.find_element_by_link_text("Home")
                    #print "home"
                    home.click()

                w = WebDriverWait(self.driver, _timeout)
                w.until(lambda driver: self.driver.find_element_by_link_text("View Cart"))
                viewcart = self.driver.find_element_by_link_text("View Cart")
                #print "view cart"
                viewcart.click()

                w = WebDriverWait(self.driver, _timeout)
                w.until(lambda driver: self.driver.find_element_by_link_text("Checkout"))
                checkout = self.driver.find_element_by_link_text("Checkout")
                #print checkout
                #print self.driver.page_source
                #print "checkout"
                checkout.click()

                #print "finding search"
                w = WebDriverWait(self.driver, _timeout)
                w.until(lambda driver: self.driver.find_element_by_link_text("Search"))
                home = self.driver.find_element_by_link_text("Search")
                #print "search"
                home.click()

                # Performing garbage collection
                collected=gc.collect()
                #print "Garbage collector: collected %d objects." % (collected)
                #print datetime.now()
            except NameError as ne:
                print "PHP Demo - NameError: {0}".format(ne)
                exit()
            except:
                print "PHP Demo - Unexpected error:", sys.exc_info()[0]
                #print self.driver.page_source

